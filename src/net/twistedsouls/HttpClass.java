/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.twistedsouls;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.URL;
import java.net.Proxy;

/**
 *
 * @author chris
 */
public class HttpClass {

    public static boolean testProxy(String IP, int Port, String PType) throws IOException {
        try {
            URL myURL = new URL("http://google.com");
            Proxy theProxy = null;
            if (PType.equalsIgnoreCase("http")) {
                theProxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(IP, Port));
            } else if (PType.equalsIgnoreCase("socks")) {
                theProxy = new Proxy(Proxy.Type.SOCKS, new InetSocketAddress(IP, Port));
            }
            HttpURLConnection conn = (HttpURLConnection) myURL.openConnection(theProxy);
            conn.setRequestProperty("Host", "google.com"); //Needs to change to "domain.tld" dynamically
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Linux; U; Android 0.5; en-us) AppleWebKit/522+ (KHTML, like Gecko) Safari/419.3");
            conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
            conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            conn.setConnectTimeout(15000);
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.connect();
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line = null;
            boolean ret = false;
            while ((line = reader.readLine()) != null) {
                if (line.contains("<title>Google</title>")) {
                    ret = true;
                    break;
                }
            }
            return ret;
        } catch (java.net.SocketTimeoutException sex) { // :D
            System.out.println("Bad Proxy");
            return false;
        } catch (java.net.ConnectException CE){
            System.out.println("Bad Proxy");
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String grabIncloakProxies() throws IOException {
        BufferedReader reader = null;
        String mySrc = "ERROR";
        try {
            URL myURL = new URL("http://incloak.com/proxy-list/?anon=34");
            HttpURLConnection conn = (HttpURLConnection) myURL.openConnection();
            conn.setRequestProperty("Host", "incloak.com"); //Needs to change to "domain.tld" dynamically
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Linux; U; Android 0.5; en-us) AppleWebKit/522+ (KHTML, like Gecko) Safari/419.3");
            conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
            conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.connect();
            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line = null;
            while ((line = reader.readLine()) != null) {
                if (line.contains("Do not know how to use a proxy?")) {
                    mySrc = line;
                    break;
                }
            }
            mySrc = (mySrc.split("</span></th>"))[1];
            mySrc = mySrc.replaceAll("(?<=<th>).*?(?=</th>)", "");
            mySrc = mySrc.replaceAll("(?<=<div).*?(?=</div>)", "");
            mySrc = mySrc.replaceAll("<div", "");
            mySrc = mySrc.replaceAll("</div>", "");
            mySrc = mySrc.replaceAll("<th>", "");
            mySrc = mySrc.replaceAll("</th>", "");
            mySrc = mySrc.replaceAll("<tr>", "");
            mySrc = mySrc.replaceAll("</tr>", "");
            mySrc = mySrc.replaceAll("<td class=tdl>", "");
            mySrc = mySrc.replaceAll("</td><td><img src=\"", "-");
            mySrc = mySrc.replaceAll("\">", ";");
            mySrc = mySrc.replaceAll("(?<=<td>).*?(?=</td>)", "");
            mySrc = mySrc.replaceAll("<td>", "");
            mySrc = mySrc.replaceAll("(?<=<td class=tdr>).*?(?=</td>)", "");
            mySrc = mySrc.replaceAll("</td>", "");
            mySrc = mySrc.replaceAll("<td class=tdr>", "");
            mySrc = mySrc.replaceAll("<tr class=d>", "");
            mySrc = mySrc.replaceAll("/images", "http://incloak.com/images");
            mySrc = (mySrc.split("<"))[0];
            return mySrc;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                reader.close();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }

    }

    public static String grabFreeProxyListProxies() throws IOException {
        BufferedReader reader = null;
        StringBuilder mySrcB;
        String mySrc;
        try {
            URL myURL = new URL("http://free-proxy-list.net/");
            HttpURLConnection conn = (HttpURLConnection) myURL.openConnection();
            conn.setRequestProperty("Host", "free-proxy-list.net"); //Needs to change to "domain.tld" dynamically
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Linux; U; Android 0.5; en-us) AppleWebKit/522+ (KHTML, like Gecko) Safari/419.3");
            conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
            conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.connect();
            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line = null;
            mySrcB = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                mySrcB.append(line);
            }
            mySrc = mySrcB.toString();
            mySrc = (mySrc.split("><th>Last Checked</th></tr></thead><tbody><tr>"))[1];
            mySrc = (mySrc.split("</tr></tbody><tfoot>"))[0];
            while (mySrc.contains("<td>")) {
                mySrc = mySrc.replaceFirst("<td>", "");
                mySrc = mySrc.replaceFirst("</td><td>", ":");
                mySrc = mySrc.replaceFirst("</td>", ";");
                mySrc = mySrc.replaceFirst("(?<=<td>).*?(?=<tr>)", "");
                if (mySrc.contains("<td><tr>")) {
                    mySrc = mySrc.replaceFirst("<td><tr>", "");
                } else {
                    break;
                }
            }
            mySrc = (mySrc.split("<td>"))[0];
            mySrc = mySrc.substring(0, mySrc.length() - 1);
            return mySrc;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                reader.close();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }

    }

    public static String grabSocksProxyListProxies() throws IOException {
        BufferedReader reader = null;
        StringBuilder mySrcB;
        String mySrc;
        try {
            URL myURL = new URL("http://socks-proxy.net/");
            HttpURLConnection conn = (HttpURLConnection) myURL.openConnection();
            conn.setRequestProperty("Host", "socks-proxy.net"); //Needs to change to "domain.tld" dynamically
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Linux; U; Android 0.5; en-us) AppleWebKit/522+ (KHTML, like Gecko) Safari/419.3");
            conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
            conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.connect();
            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line = null;
            mySrcB = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                mySrcB.append(line);
            }
            mySrc = mySrcB.toString();
            mySrc = (mySrc.split("><th>Last Checked</th></tr></thead><tbody><tr>"))[1];
            mySrc = (mySrc.split("</tr></tbody><tfoot>"))[0];
            while (mySrc.contains("<td>")) {
                mySrc = mySrc.replaceFirst("<td>", "");
                mySrc = mySrc.replaceFirst("</td><td>", ":");
                mySrc = mySrc.replaceFirst("</td>", ";");
                mySrc = mySrc.replaceFirst("(?<=<td>).*?(?=<tr>)", "");
                if (mySrc.contains("<td><tr>")) {
                    mySrc = mySrc.replaceFirst("<td><tr>", "");
                } else {
                    break;
                }
            }
            mySrc = (mySrc.split("<td>"))[0];
            mySrc = mySrc.substring(0, mySrc.length() - 1);
            return mySrc;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                reader.close();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }

    }

    public static String getRandomName(String url) throws Exception {
        //http://www.namegenerator.biz/application/p.php?type=4&id=fantasy_female_names&id2=fantasy_male_names&spaceflag=false
        BufferedReader reader = null;
        StringBuilder mySrcB;
        String mySrc;
        int numOfProxies;
        String[] myProx = null;
        Proxy theProxy = null;
        String hostURL;
        String myName;
        try {
            URL myURL = new URL(url);
            hostURL = (url.replaceFirst("http://", "").split("/"))[0];
            HttpURLConnection conn = (HttpURLConnection) myURL.openConnection();
            conn.setRequestProperty("Host", hostURL); //Needs to change to "domain.tld" dynamically
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Linux; U; Android 0.5; en-us) AppleWebKit/522+ (KHTML, like Gecko) Safari/419.3");
            conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
            conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            conn.setRequestProperty("Referer", url); //Needs to be changed or removed
            conn.setConnectTimeout(30000);
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);

            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line = null;
            mySrcB = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                mySrcB.append(line);
            }
            mySrc = mySrcB.toString();
            mySrc = mySrc.substring(0, mySrc.length() - 1);
            String[] myNames = mySrc.split(",");
            myName = myNames[0 + (int) (Math.random() * (myNames.length - 1))];
            myName = myName + Integer.toString(0 + (int) (Math.random() * 9999));
            return myName;
        } catch (java.net.SocketTimeoutException sex) { // :D
            return "ERR:Timed;";
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                reader.close();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    public static String submitForm(String url, String formData) throws Exception {
        BufferedReader reader = null;
        StringBuilder mySrcB;
        String mySrc;
        int numOfProxies;
        String[] myProx = null;
        Proxy theProxy = null;
        String hostURL;
        try {
            URL myURL = new URL(url);
            hostURL = (url.replaceFirst("http://", "").split("/"))[0];
            Main.SQL.Connect();
            numOfProxies = Main.SQL.getNumProxies() - 1;
            Main.SQL.Close();
            if (numOfProxies == -1) {
                return "No Proxies";
            }
            Main.SQL.Connect();
            myProx = Main.SQL.getProxyByNum(0 + (int) (Math.random() * numOfProxies));
            Main.SQL.Close();
            if (myProx[2].contains("HTTP") || myProx[2].contains("http")) {
                theProxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(myProx[0], Integer.parseInt(myProx[1])));
            }
            HttpURLConnection conn = (HttpURLConnection) myURL.openConnection(theProxy);
            conn.setRequestProperty("Host", hostURL); //Needs to change to "domain.tld" dynamically
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Linux; U; Android 0.5; en-us) AppleWebKit/522+ (KHTML, like Gecko) Safari/419.3");
            conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
            conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            conn.setRequestProperty("Referer", url);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("Content-Length", "" + Integer.toString(formData.getBytes().length));
            conn.setConnectTimeout(30000);
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);

            //Send request
            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes(formData);
            wr.flush();
            wr.close();

            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line = null;
            mySrcB = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                mySrcB.append(line + "\n");
            }
            mySrc = mySrcB.toString();
            Main.mainScr.consoleBox.append("Finished on proxy: " + myProx[0] + ":" + myProx[1] + "\n");
            return mySrc;
        } catch (java.net.SocketTimeoutException sex) { // :D
            Main.SQL.deleteRow("DELETE FROM proxies WHERE IP='" + myProx[0] + "' AND Port=" + Integer.parseInt(myProx[1]) + " AND Type='" + myProx[2] + "';");
            Main.SQL.insertProxy("badproxies", myProx[0], Integer.parseInt(myProx[1]), myProx[2]);
            Main.mainScr.consoleBox.append("Proxy moved to bad proxy list(timed out). IP: " + myProx[0] + " - Port: " + myProx[1] + "\n");
            return "ERR:Timed;";
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                reader.close();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }
}
