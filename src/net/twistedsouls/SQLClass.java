/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.twistedsouls;

/**
 *
 * @author chris
 */
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SQLClass {
    
    protected String DBName = null;
    protected Connection Con = null;
    protected Statement State = null;
    protected ResultSet Results = null;
    protected boolean needsSetup = true;
    protected boolean neededSetup = false;
    protected boolean isDBLocked = false;
    
    public SQLClass(String DBName){
        this.DBName = DBName;
    }
    
    public void Connect(){
        isDBLocked = true;
        try {
            if(!needsSetup){
                if(Con.isValid(5))
                    Con.close();
            }
            Class.forName("org.sqlite.JDBC");
            Con = DriverManager.getConnection("jdbc:sqlite:" + DBName + ".db");
            Con.setAutoCommit(false);
        }catch (SQLException e) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SQLClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(needsSetup)
            checkSetup();
    }
    
    public void checkSetup(){
        try {
            DatabaseMetaData meta = Con.getMetaData();
            ResultSet res = meta.getTables(null, null, null,
                    new String[] {"TABLE"});
            while (res.next()) {
                if((res.getString("TABLE_NAME")).equalsIgnoreCase("proxies")){
                    needsSetup = false;
                }
            }
            res.close();
            if(needsSetup)
                setupDB();
        } catch (SQLException ex) {
            Logger.getLogger(SQLClass.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void deleteRow(String sql){
        Connect();
        try {
            State = Con.createStatement();
            State.executeUpdate(sql);
            State.close();
            Con.commit();
            Close();
        } catch (SQLException ex) {
            Logger.getLogger(SQLClass.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void setupDB(){
        System.out.println("Running DB Setup!");
        neededSetup = true;
        try {
            State = Con.createStatement();
            String sql = "CREATE TABLE proxies " +
                    "(IP           CHAR(50), " +
                    " Port         INT, " +
                    " Type         CHAR(50))";
            State.executeUpdate(sql);
            sql = "CREATE TABLE badproxies " +
                    "(IP           CHAR(50), " +
                    " Port         INT, " +
                    " Type         CHAR(50))";
            State.executeUpdate(sql);
            sql = "CREATE TABLE accounts " +
                    "(Name           CHAR(50), " +
                    " Pass            CHAR(50), " +
                    " EMail            CHAR(50), " +
                    " Website            CHAR(50), " +
                    " IP            CHAR(50), " +
                    " Port         INT)";
            State.executeUpdate(sql);
            sql = "CREATE TABLE websites " +
                    "(Website           CHAR(50), " +
                    " FormData            CHAR(500), " +
                    " SucText         CHAR(500))";
            State.executeUpdate(sql);
            State.close();
            Con.commit();
            needsSetup = false;
        } catch (SQLException ex) {
            Logger.getLogger(SQLClass.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void Close(){
        try {
            Con.close();
        } catch (SQLException ex) {
            Logger.getLogger(SQLClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        isDBLocked = false;
    }
   
    public Object[][] getTableData(String table){
        Object[][] obj;
        int rowNum = 0;
        try {
            State = Con.createStatement();
            Results = State.executeQuery("SELECT * FROM " + table + ";");
            while(Results.next()){
                rowNum++;
            }
            Results.close();
            State.close();
            obj = new Object[rowNum][3];
            rowNum = 0;
            State = Con.createStatement();
            Results = State.executeQuery("SELECT * FROM " + table + ";");
            while(Results.next()){
                if(table.equalsIgnoreCase("proxies") || table.equalsIgnoreCase("badproxies") ){
                    obj[rowNum][0] = Results.getString(1);
                    obj[rowNum][1] = Results.getInt(2);
                    obj[rowNum][2] = Results.getString(3);
                }
                rowNum++;
            }
            Results.close();
            State.close();
            return obj;
        } catch (SQLException ex) {
            Logger.getLogger(SQLClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public int getNumProxies(){
        int rowNum = 0;
        try {
            State = Con.createStatement();
            Results = State.executeQuery("SELECT * FROM proxies;");
            while(Results.next()){
                rowNum++;
            }
            Results.close();
            State.close();
            return rowNum;
        } catch (SQLException ex) {
            Logger.getLogger(SQLClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }
    
    public String[] getProxyByNum(int num){
        int rowNum = 0;
        String[] proxy = new String[3];
        try {
            State = Con.createStatement();
            Results = State.executeQuery("SELECT * FROM proxies;");
            while(Results.next()){
                if(rowNum == num){
                    proxy[0] = Results.getString(1);
                    proxy[1] = Results.getString(2);
                    proxy[2] = Results.getString(3);
                }
                rowNum++;
            }
            Results.close();
            State.close();
            return proxy;
        } catch (SQLException ex) {
            Logger.getLogger(SQLClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void insertProxy(String table, String ip, int port, String type){
        Connect();
        try {
            State = Con.createStatement();
            State.executeUpdate("INSERT INTO " + table + " (IP, Port, Type) VALUES ('"+ip+"', "+port+", '" + type + "');");
            State.close();
            Con.commit();
            Close();
        } catch (SQLException ex) {
            Logger.getLogger(SQLClass.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void insertWebsite(String website, String data, String sucText){
        Connect();
        try {
            State = Con.createStatement();
            State.executeUpdate("INSERT INTO websites (Website, FormData, SucText) VALUES ('"+website+"', '"+data+"', '"+sucText+"');");
            State.close();
            Con.commit();
            Close();
        } catch (SQLException ex) {
            Logger.getLogger(SQLClass.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void fillWebsites(javax.swing.JComboBox cb){
        Connect();
        try {
            State = Con.createStatement();
            Results = State.executeQuery("SELECT * FROM websites;");
            cb.removeAllItems();
            while(Results.next()){
                cb.addItem(Results.getString(1));
            }
            Results.close();
            State.close();
            Close();
        } catch (SQLException ex) {
            Logger.getLogger(SQLClass.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String[] getWebsite(String web){
        Connect();
        boolean found = false;
        String[] resul = null;
        try {
            State = Con.createStatement();
            Results = State.executeQuery("SELECT * FROM websites;");
            while(Results.next()){
                if(Results.getString(1).equalsIgnoreCase(web)){
                    resul = new String[3];
                    resul[0] = Results.getString(1);
                    resul[1] = Results.getString(2);
                    resul[2] = Results.getString(3);
                    break;
                }
            }
            Results.close();
            State.close();
            Close();
            return resul;
        } catch (SQLException ex) {
            Logger.getLogger(SQLClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public boolean doesProxyExist(String ip, int port, String type){
        Connect();
        try {
            State = Con.createStatement();
            Results = State.executeQuery("SELECT * FROM proxies WHERE Type='"+type+"';");
            while(Results.next()){
                if(Results.getString(1).equalsIgnoreCase(ip) && Results.getString(3).equalsIgnoreCase(type) && (Results.getInt(2) == port)){
                    Results.close();
                    State.close();
                    Close();
                    return true;
                }
            }
            Results.close();
            State.close();
            Close();
            return false;
        } catch (SQLException ex) {
            Logger.getLogger(SQLClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
}
