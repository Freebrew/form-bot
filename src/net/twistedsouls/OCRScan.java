/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.twistedsouls;

import java.io.File;

import com.github.axet.lookup.*;
import com.github.axet.lookup.common.ImageBinaryGrey;
import java.awt.image.BufferedImage;

public class OCRScan{

    public OCR l = null;
    private String str = "";
    
    public OCRScan() {
        l = new OCR(0.70f);

        // will go to com/github/axet/lookup/fonts folder and load all font
        // familys (here is only font_1 family in this library)
        //l.loadFontsDirectory(OCRScan.class, new File("fonts"));

        // example how to load only one family
        // "com/github/axet/lookup/fonts/font_1"
        l.loadFont(OCRScan.class, new File("fonts", "font_2"));

        // recognize using all familys set
        //str = l.recognize(Capture.load(OCRScan.class, "test4.png"));
        //System.out.println(str);

        // recognize using only one family set
        //str = l.recognize(Capture.load(OCRScan.class, "test3.png"), "font_1");
        //System.out.println(str);

        // recognize using only one family set and rectangle
        //ImageBinaryGrey i = new ImageBinaryGrey(Capture.load(OCRScan.class, "full.png"));
        //str = l.recognize(i, 1285, 654, 1343, 677, l.getSymbols("font_1"));
        //System.out.println(str);
    }
    
    public void scanImage(BufferedImage bi){
        str = l.recognize(bi);
        System.out.println(str);
    }
}
