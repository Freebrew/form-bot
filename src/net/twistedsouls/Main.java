/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.twistedsouls;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;

/**
 *
 * @author chris
 */
public class Main {

    public static initScreen initScr;
    public static mainForm mainScr;
    public static SQLClass SQL;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        CookieHandler.setDefault(new CookieManager( null, CookiePolicy.ACCEPT_ALL));
        SQL = new SQLClass("DynamicACForms");
        SQL.Connect();
        SQL.Close();
        if(SQL.neededSetup){
            initScr = new initScreen();
            initScr.setLocationRelativeTo(null);
            initScr.setVisible(true);
        }else{
            mainScr = new mainForm();
        }
    }
    
}
